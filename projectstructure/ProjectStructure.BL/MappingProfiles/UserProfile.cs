﻿using AutoMapper;
using ProjectStructure.Common.DTO;
using ProjectStructure.Common.DTO.QueryDTO;
using ProjectStructure.DAL.Models;

namespace ProjectStructure.BL.MappingProfiles
{
    public class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<UserDTO, User>();
            CreateMap<User, UserDTO>();
            CreateMap<User, UserTasksDTO>();
            CreateMap<UserTasksDTO, User>();
        }
    }
}
