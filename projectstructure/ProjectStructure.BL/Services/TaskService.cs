﻿using ProjectStructure.BL.Interfaces;
using ProjectStructure.Common.DTO;
using ProjectStructure.DAL.UnitOfWork.Interfaces;
using System.Collections.Generic;
using AutoMapper;

namespace ProjectStructure.BL.Services
{
    public class TaskService : ITaskService
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IRepository<DAL.Models.Task> _taskRepository;

        public TaskService(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
            _taskRepository = _unitOfWork.TaskRepository;
        }
        public void CreateTask(TaskDTO taskDTO)
        {
            var taskEntity = _mapper.Map<DAL.Models.Task>(taskDTO);
            _taskRepository.Create(taskEntity);
            //save changes
        }

        public void DeleteTask(int taskId)
        {
            _taskRepository.Delete(taskId);
            //save changes
        }

        public IEnumerable<TaskDTO> GetAllTasks()
        {
            var alltasks = _taskRepository.GetAll();
            return _mapper.Map<IEnumerable<TaskDTO>>(alltasks);
        }

        public TaskDTO GetTaskById(int taskId)
        {
            var taskEntity = _taskRepository.Get(taskId);
            return _mapper.Map<TaskDTO>(taskEntity);
        }

        public void UpdateTask(TaskDTO taskDTO)
        {
            var taskEntity = _mapper.Map<DAL.Models.Task>(taskDTO);
            _taskRepository.Update(taskEntity);
            //save changes
        }
    }
}
