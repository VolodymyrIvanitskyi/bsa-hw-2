﻿using AutoMapper;
using ProjectStructure.BL.Interfaces;
using ProjectStructure.Common.DTO;
using ProjectStructure.DAL.Models;
using ProjectStructure.DAL.UnitOfWork.Interfaces;
using System.Collections.Generic;

namespace ProjectStructure.BL.Services
{
    public class ProjectService : IProjectService
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IRepository<Project> _projectRepository;

        public ProjectService(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
            _projectRepository = _unitOfWork.ProjectRepository;
        }
        public void CreateProject(ProjectDTO projectDTO)
        {
            var projectEntity = _mapper.Map<Project>(projectDTO);
            _projectRepository.Create(projectEntity);
            //save changes
        }

        public void DeleteProject(int projectId)
        {
            _projectRepository.Delete(projectId);
            //save changes
        }

        public IEnumerable<ProjectDTO> GetAllProjects()
        {
            var allProjects = _projectRepository.GetAll();
            return _mapper.Map<IEnumerable<ProjectDTO>>(allProjects);
        }

        public ProjectDTO GetProjectById(int projectId)
        {
            var projectEntity = _projectRepository.Get(projectId);
            return _mapper.Map<ProjectDTO>(projectEntity);
        }

        public void UpdateProject(ProjectDTO projectDTO)
        {
            var projectEntity = _mapper.Map<Project>(projectDTO);
            _projectRepository.Update(projectEntity);
            //save changes
        }
    }
}

