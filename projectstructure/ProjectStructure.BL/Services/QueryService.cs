﻿using AutoMapper;
using ProjectStructure.BL.Exceptions;
using ProjectStructure.BL.Interfaces;
using ProjectStructure.Common.DTO;
using ProjectStructure.Common.DTO.QueryDTO;
using ProjectStructure.DAL.Models;
using ProjectStructure.DAL.UnitOfWork.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProjectStructure.BL.Services
{
    public class QueryService : IQueryService
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        private readonly List<Project> _projects;
        private readonly List<Team> _teams;
        private readonly List<Task> _tasks;
        private readonly List<User> _users;

        public QueryService(IMapper mapper, IUnitOfWork unitOfWork)
        {
            this._mapper = mapper;
            this._unitOfWork = unitOfWork;
            this._projects = _unitOfWork.ProjectRepository.GetAll().ToList();
            this._teams = _unitOfWork.TeamRepository.GetAll().ToList();
            this._tasks = _unitOfWork.TaskRepository.GetAll().ToList();
            this._users = _unitOfWork.UserRepository.GetAll().ToList();
        }
        public Dictionary<int, int> GetCountTasksByUser(int authorId)
        {
            if (!IsUserExist(authorId))
            {
                throw new NotFoundException(nameof(User), authorId);
            }

            return _projects
                .Where(x => x.AuthorId == authorId)
                .GroupJoin(_tasks,
                project => project.Id,
                task => task.ProjectId,
                (project, tasks) => new {
                    projectId = project.Id,
                    countOfTasks = tasks.Count()
                })
                .ToDictionary(d => d.projectId, d => d.countOfTasks);
        }

        public IEnumerable<DataFromProjectDTO> GetDataFromProject()
        {
            var aboutProject = _teams
                .GroupJoin(
                _users,
                t => t.Id,
                u => u.TeamId,
                (team, u) =>
                {
                    team.Users = u.ToList();
                    return team;
                })
                .Join(
                _projects,
                t => t.Id,
                p => p.TeamId,
                (t, project) =>
                {
                    project.Team = t;
                    return project;
                })
                .GroupJoin(
                _tasks,
                p => p.Id,
                t => t.ProjectId,
                (project, t) =>
                {
                    project.Tasks = t.ToList();
                    return project;
                })
                .Select(
                project => new DataFromProjectDTO()
                {
                    Project = _mapper.Map<ProjectDTO>(project),

                    TheLongestTask = _mapper.Map<TaskDTO>(project
                    .Tasks?
                    .OrderByDescending(t => t.Description.Length)
                    .FirstOrDefault()),

                    TheShortestTask = _mapper.Map<TaskDTO>(project
                    .Tasks?
                    .OrderBy(t => t.Name.Length)
                    .FirstOrDefault()),

                    CountOfUsers = project.Tasks.Count < 3
                    || project.Description.Length > 20
                    ? project.Team.Users?.Count : 0
                }).OrderBy(p => p.Project.Id).ToList();

            return aboutProject;

        }

        public DataFromUserDTO GetDatafromUser(int userId)
        {
            if (!IsUserExist(userId))
            {
                throw new NotFoundException(nameof(User), userId);
            }

            DataFromUserDTO data =  new DataFromUserDTO()
            {
                User = _mapper.Map<UserDTO>(_users
                    .Where(u => u.Id == userId)
                    .FirstOrDefault()),

                LastProject = _mapper.Map<ProjectDTO>(_projects
                    .Where(project => project?.AuthorId == userId)
                    .OrderByDescending(x => x.CreatedAt)
                    .FirstOrDefault()),

                CountOfTasks = _tasks
                    .Where(task => task.ProjectId == _projects
                    .Where(project => project?.AuthorId == userId)
                    .OrderByDescending(project => project.CreatedAt)
                    .FirstOrDefault()?.Id
                    ).Count(),

                CountOfCanceledTasks = _tasks
                    .Where(t => t.PerformerId == userId && (t.State == TaskState.Canceled || t.State == TaskState.Started))
                    .Count(),

                TheLongestTask = _mapper.Map<TaskDTO>(_tasks
                    .Where(task => task.PerformerId == userId)
                    .OrderByDescending(task => task.FinishedAt - task.CreatedAt)
                    .FirstOrDefault())
            };

            return data;
        }

        public IEnumerable<TaskDTO> GetFinishedTasksForUser(int userId)
        {
            if (!IsUserExist(userId))
            {
                throw new NotFoundException(nameof(User), userId);
            }

            return _tasks
                .Where(t => t.State == TaskState.Finished && t.PerformerId == userId && t.FinishedAt?.Year == 2021)
                .Select(t => new TaskDTO { Id = t.Id, Name = t.Name })
                .ToList();
        }

        public IEnumerable<TaskDTO> GetTasksForUser(int userId)
        {
            if (!IsUserExist(userId))
            {
                throw new NotFoundException(nameof(User), userId);
            }

            var tasks= _tasks
                .Where(t => t.PerformerId == userId && t.Name.Length < 45)
                .ToList();
            return _mapper.Map<IEnumerable<TaskDTO>>(tasks);
        }

        public IEnumerable<OlderTeamDTO> GetTeamsWhenMembersOlderThan10Years()
        {
            return _teams
                .GroupJoin(_users,
                team => team.Id,
                user => user.TeamId,
                (team, users) =>
                new OlderTeamDTO()
                {
                    Id = team.Id,
                    Name = team.Name,
                    Users = _mapper.Map<List<UserDTO>>(users.Where(user => (DateTime.Now.Year - user.BirthDay.Year) > 10).ToList())
                }).ToList();
        }

        public IEnumerable<UserTasksDTO> GetUsersAlphabetically()
        {
            return _mapper.Map<List<UserTasksDTO>>(_users.GroupJoin(
                _tasks,
                user => user.Id,
                task => task.PerformerId,
                (user, tasks) => {
                    user.Tasks = tasks.OrderByDescending(x => x.Name.Length).ToList();
                    return user;
                })
                .OrderBy(user => user.FirstName)
                .ToList());
        }

        private bool IsUserExist(int userId)
        {
            User user = _users
                    .Where(u => u.Id == userId)
                    .FirstOrDefault();

            if (user is null)
                return false;
            else
                return true;
        }
    }
}
