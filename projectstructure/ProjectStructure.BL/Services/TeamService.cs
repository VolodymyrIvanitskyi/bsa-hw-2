﻿using System.Collections.Generic;
using AutoMapper;
using ProjectStructure.BL.Interfaces;
using ProjectStructure.Common.DTO;
using ProjectStructure.DAL.Models;
using ProjectStructure.DAL.UnitOfWork.Interfaces;

namespace ProjectStructure.BL.Services
{
    public class TeamService : ITeamService
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IRepository<Team> _teamRepository;

        public TeamService(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
            _teamRepository = _unitOfWork.TeamRepository;
        }
        public void CreateTeam(TeamDTO teamDTO)
        {
            var teamEntity = _mapper.Map<Team>(teamDTO);
            _teamRepository.Create(teamEntity);
            //save changes
        }

        public void DeleteTeam(int teamId)
        {
            _teamRepository.Delete(teamId);
            //save changes
        }

        public IEnumerable<TeamDTO> GetAllTeams()
        {
            var allTeams = _teamRepository.GetAll();
            return _mapper.Map<IEnumerable<TeamDTO>>(allTeams);
        }

        public TeamDTO GetTeamById(int teamId)
        {
            var teamEntity = _teamRepository.Get(teamId);
            return _mapper.Map<TeamDTO>(teamEntity);
        }

        public void UpdateTeam(TeamDTO teamDTO)
        {
            var teamEntity = _mapper.Map<Team>(teamDTO);
            _teamRepository.Update(teamEntity);
        }
    }
}
