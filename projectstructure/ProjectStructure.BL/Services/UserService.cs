﻿using System.Collections.Generic;
using AutoMapper;
using ProjectStructure.BL.Interfaces;
using ProjectStructure.Common.DTO;
using ProjectStructure.DAL.Models;
using ProjectStructure.DAL.UnitOfWork.Interfaces;

namespace ProjectStructure.BL.Services
{
    public class UserService : IUserService
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IRepository<User> _userRepository;

        public UserService(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
            _userRepository = _unitOfWork.UserRepository;
        }

        public void CreateUser(UserDTO userDTO)
        {
            var userEntity = _mapper.Map<User>(userDTO);
            _userRepository.Create(userEntity);
            //save changes
        }

        public void DeleteUser(int userId)
        {
            _userRepository.Delete(userId);
            //save changes
        }

        public IEnumerable<UserDTO> GetAllUsers()
        {
            var allUsers = _userRepository.GetAll();
            return _mapper.Map<IEnumerable<UserDTO>>(allUsers);
        }

        public UserDTO GetUserById(int userId)
        {
            var userEntity = _userRepository.Get(userId);
            return _mapper.Map<UserDTO>(userEntity);
        }

        public void UpdateUser(UserDTO userDTO)
        {
            var userEntity = _mapper.Map<User>(userDTO);
            _userRepository.Update(userEntity);
            //save changes
        }
    }
}
