﻿using ProjectStructure.Common.DTO;
using System.Collections.Generic;
using ProjectStructure.Common.DTO.QueryDTO;

namespace ProjectStructure.BL.Interfaces
{
    public interface IQueryService
    {
        public IEnumerable<TaskDTO> GetTasksForUser(int userId);
        public Dictionary<int, int> GetCountTasksByUser(int userId);
        public IEnumerable<TaskDTO> GetFinishedTasksForUser(int userId);
        public IEnumerable<OlderTeamDTO> GetTeamsWhenMembersOlderThan10Years();
        public IEnumerable<UserTasksDTO> GetUsersAlphabetically();
        public DataFromUserDTO GetDatafromUser(int id);
        public IEnumerable<DataFromProjectDTO> GetDataFromProject();


    }
}
