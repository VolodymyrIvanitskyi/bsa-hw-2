﻿using ProjectStructure.Common.DTO;
using System.Collections.Generic;

namespace ProjectStructure.BL.Interfaces
{
    public interface ITaskService
    {
        IEnumerable<TaskDTO> GetAllTasks();
        TaskDTO GetTaskById(int id);
        void CreateTask(TaskDTO task);
        void UpdateTask(TaskDTO task);
        void DeleteTask(int taskId);
    }
}
