﻿
namespace ProjectStructure.Common.DTO.QueryDTO
{
    public class DataFromUserDTO
    {
        public UserDTO User { get; set; }
        public ProjectDTO LastProject { get; set; }
        public int? CountOfTasks { get; set; }
        public int? CountOfCanceledTasks { get; set; }
        public TaskDTO TheLongestTask { get; set; }
    }
}
