﻿
namespace ProjectStructure.Common.DTO
{
    public enum TaskStateDTO : int
    {
        Created,
        Started,
        Completed,
        Canceled
    }
}
