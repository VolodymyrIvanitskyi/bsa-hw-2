﻿using ProjectStructure.DAL.Models;
using ProjectStructure.DAL.UnitOfWork.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ProjectStructure.DAL.UnitOfWork.Reposity
{
	public class TeamRepository : IRepository<Team>
	{
		private static readonly List<Team> teams;

		static TeamRepository()
		{
			teams = new List<Team>()
			{
				new Team()
				{
					Id = 0,
					Name = "Denesik - Greenfelder",
					CreatedAt = DateTime.Parse("2019-08-25T15:48:06.062331+00:00")

				},
				new Team()
				{
					Id = 1,
					Name = "Durgan Group",
					CreatedAt = DateTime.Parse("2017-03-31T02:29:28.3740504+00:00")

				},
				new Team()
				{
					Id = 2,
					Name = "Kassulke LLC",
					CreatedAt = DateTime.Parse("2019-02-21T15:47:30.3797852+00:00")

				},
				new Team()
				{
					Id = 3,
					Name = "Harris LLC",
					CreatedAt = DateTime.Parse("2018-08-28T08:18:46.4160342+00:00")

				},
				new Team()
				{
					Id = 4,
					Name = "Mitchell Inc",
					CreatedAt = DateTime.Parse("2019-04-03T09:58:33.0178179+00:00")
				},
				new Team()
				{
					Id = 5,
					Name = "Smitham Group",
					CreatedAt = DateTime.Parse("2016-10-05T07:57:02.8427653+00:00")
				},
				new Team()
				{
					Id = 6,
					Name = "Kutch - Roberts",
					CreatedAt = DateTime.Parse("2016-10-31T05:05:15.1076578+00:00")

				},
				new Team()
				{
					Id = 7,
					Name = "Parisian Group",
					CreatedAt = DateTime.Parse("2016-07-17T01:34:55.0917082+00:00")
				}
			};
		}

		public Team Create(Team entity)
		{
			if (entity is not null)
			{
				teams.Add(entity);
				return entity;
			}
			else
			{
				throw new Exception("Object is null :" + typeof(Team).ToString());
			}
		}

		public void Delete(int id)
		{
			var item = teams.FirstOrDefault(i => i.Id == id);

			if (item is null)
			{
				throw new Exception("Not found " + typeof(Team).ToString());
			}
			teams.Remove(item);
		}

		public Team Get(int id)
		{
			var item = teams.FirstOrDefault(i => i.Id == id);

			if (item is null)
			{
				throw new Exception("Not found " + typeof(Team).ToString());
			}
			return item;
		}

		public IEnumerable<Team> GetAll()
		{
			return teams;
		}

		public Team Update(Team entity)
		{
			if (entity is null)
				throw new Exception("Not found " + typeof(Team).ToString());

			var item = teams.FirstOrDefault(i => i.Id == entity.Id);

			if (item is null)
				throw new Exception("Not found " + typeof(Team).ToString());
			else
			{
				teams.Remove(item);
				teams.Add(entity);
			}

			return entity;
		}
	}
}
