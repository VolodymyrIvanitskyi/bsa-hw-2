﻿using ProjectStructure.DAL.Models;
using ProjectStructure.DAL.UnitOfWork.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ProjectStructure.DAL.UnitOfWork.Reposity
{
	public class TaskRepository : IRepository<Models.Task>
	{
		private static readonly List<Models.Task> tasks;
		static TaskRepository()
		{
			tasks = new List<Models.Task>()
			{
				new Models.Task()
				{
					Id = 2,
					ProjectId = 2,
					PerformerId = 2,
					Name = "product Direct utilize",
					Description = "Eum a eum.",
					State = (TaskState)2,
					CreatedAt = DateTime.Parse("2019-02-15T15:06:52.0600666+00:00"),
					FinishedAt = null
				},
				new Models.Task()
				{
					Id = 3,
					ProjectId = 2,
					PerformerId = 3,
					Name = "bypass",
					Description = "Sint voluptatem quas.",
					State = (TaskState)2,
					CreatedAt = DateTime.Parse("2017-08-16T06:13:44.5773845+00:00"),
					FinishedAt = DateTime.Parse("2020-09-09T06:34:47.460216+00:00")
				},
				new Models.Task()
				{
					Id = 4,
					ProjectId = 2,
					PerformerId = 4,
					Name = "withdrawal contextually-based",
					Description = "Delectus quibusdam id quia iure neque maiores molestias sed aut.",
					State = (TaskState)2,
					CreatedAt = DateTime.Parse("2018-10-19T00:58:34.8045103+00:00"),
					FinishedAt = null
				},
				new Models.Task()
				{
					Id = 5,
					ProjectId = 2,
					PerformerId = 3,
					Name = "mobile Organized",
					Description = "Earum blanditiis repellendus qui magni aliquam quisquam consequatur odio ducimus.",
					State = (TaskState)2,
					CreatedAt = DateTime.Parse("2018-06-15T06:03:48.0732466+00:00"),
					FinishedAt = null
				},
				new Models.Task()
				{
					Id = 6,
					ProjectId = 2,
					PerformerId = 2,
					Name = "world-class Circles",
					Description = "Reiciendis iusto rerum non et aut eaque.",
					State = (TaskState)2,
					CreatedAt = DateTime.Parse("2020-05-21T14:56:53.8117818+00:00"),
					FinishedAt = DateTime.Parse("2021-01-21T14:56:53.8117818+00:00")
				},
				new Models.Task()
				{
					Id = 7,
					ProjectId = 2,
					PerformerId = 4,
					Name = "Automotive & Tools transitional bifurcated",
					Description = "Et rerum ad.",
					State = (TaskState)2,
					CreatedAt = DateTime.Parse("2018-07-10T16:21:12.0886153+00:00"),
					FinishedAt = null
				}
			};
		}
		public Models.Task Create(Models.Task entity)
		{
			if (entity is not null)
			{
				tasks.Add(entity);
				return entity;
			}
			else
			{
				throw new Exception("Object is null :" + typeof(Models.Task).ToString());
			}
		}

		public void Delete(int id)
		{
			var item = tasks.FirstOrDefault(i => i.Id == id);

			if (item is null)
			{
				throw new Exception("Not found " + typeof(Models.Task).ToString());
			}
			tasks.Remove(item);
		}

		public Models.Task Get(int id)
		{
			var item = tasks.FirstOrDefault(i => i.Id == id);

			if (item is null)
			{
				throw new Exception("Not found " + typeof(Models.Task).ToString());
			}
			return item;
		}

		public IEnumerable<Models.Task> GetAll()
		{
			return tasks;
		}

		public Models.Task Update(Models.Task entity)
		{
			if (entity is null)
				throw new Exception("Not found " + typeof(Models.Task).ToString());

			var item = tasks.FirstOrDefault(i => i.Id == entity.Id);

			if (item is null)
				throw new Exception("Not found " + typeof(Models.Task).ToString());
			else
			{
				tasks.Remove(item);
				tasks.Add(entity);
			}

			return entity;
		}
	}
}
