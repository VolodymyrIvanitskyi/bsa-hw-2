﻿using ProjectStructure.DAL.Models;
using ProjectStructure.DAL.UnitOfWork.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ProjectStructure.DAL.UnitOfWork.Reposity
{
	public class UserRepository : IRepository<User>
	{
		private static readonly List<User> users;
		static UserRepository()
		{
			users = new List<User>()
			{
				new User()
				{
					Id=0,
					TeamId = 0,
					FirstName = "Vivian",
					LastName = "Martz",
					Email = "Vivian99@yahoo.com",
					RegisteredAt = DateTime.Parse("2018-10-18T22:37:40.7562662+00:00"),
					BirthDay = DateTime.Parse("1953-12-23T02:31:55.625118+00:00"),
				},
				new User()
				{
					Id = 1,
					TeamId = 0,
					FirstName = "Theresa",
					LastName = "Gottlieb",
					Email = "Theresa_Gottlieb66@yahoo.com",
					RegisteredAt = DateTime.Parse("2019-12-04T16:52:04.3727619+00:00"),
					BirthDay = DateTime.Parse("1992-09-27T20:27:03.5236015+00:00"),

				},
				new User()
				{
					Id=2,
					TeamId = 0,
					FirstName = "Brandy",
					LastName = "Witting",
					Email = "Brandy.Witting@gmail.com",
					RegisteredAt = DateTime.Parse("2019-01-10T02:51:56.7148896+00:00"),
					BirthDay = DateTime.Parse("2007-01-01T05:10:18.898869+00:00"),


				},
				new User()
				{
					Id=3,
					TeamId = 0,
					FirstName = "Theresa",
					LastName = "Ebert",
					Email = "Theresa82@hotmail.com",
					RegisteredAt = DateTime.Parse("2017-05-14T02:37:44.4486766+00:00"),
					BirthDay = DateTime.Parse("1980-02-20T16:32:12.6358667+00:00")
				},
				new User()
				{
					Id=4,
					TeamId = 0,
					FirstName = "Alfredo",
					LastName = "Simonis",
					Email = "Alfredo_Simonis@yahoo.com",
					RegisteredAt = DateTime.Parse("2019-10-14T16:40:52.2772028+00:00"),
					BirthDay = DateTime.Parse("1954-04-09T05:04:50.4709098+00:00")

				},
				new User()
				{
					Id=5,
					TeamId = 0,
					FirstName = "Joanna",
					LastName = "Botsford",
					Email = "Joanna25@hotmail.com",
					RegisteredAt = DateTime.Parse("2019-05-14T18:02:44.0995821+00:00"),
					BirthDay = DateTime.Parse("2009-05-12T02:09:16.2373321+00:00")
				},
				new User()
				{
					Id=6,
					TeamId = 0,
					FirstName = "Ann",
					LastName = "Langworth",
					Email = "Ann.Langworth@hotmail.com",
					RegisteredAt = DateTime.Parse("2017-08-09T05:47:42.3699036+00:00"),
					BirthDay = DateTime.Parse("1962-02-26T08:08:59.071182+00:00")
				},
				new User()
				{
					Id=7,
					TeamId = 0,
					FirstName = "Christie",
					LastName = "Gusikowski",
					Email = "Christie.Gusikowski@hotmail.com",
					RegisteredAt = DateTime.Parse("2019-10-07T07:41:43.2460699+00:00"),
					BirthDay = DateTime.Parse("1981-01-10T11:38:30.5290222+00:00")
				}
			};
		}
		public User Create(User entity)
		{
			if (entity is not null)
			{
				users.Add(entity);
				return entity;
			}
			else
			{
				throw new Exception("Object is null :" + typeof(User).ToString());
			}
		}

		public void Delete(int id)
		{
			var item = users.FirstOrDefault(i => i.Id == id);

			if (item is null)
			{
				throw new Exception("Not found " + typeof(User).ToString());
			}
			users.Remove(item);
		}

		public User Get(int id)
		{
			var item = users.FirstOrDefault(i => i.Id == id);

			if (item is null)
			{
				throw new Exception("Not found " + typeof(User).ToString());
			}
			return item;
		}

		public IEnumerable<User> GetAll()
		{
			return users;
		}

		public User Update(User entity)
		{
			if (entity is null)
				throw new Exception("Not found " + typeof(User).ToString());

			var item = users.FirstOrDefault(i => i.Id == entity.Id);

			if (item is null)
				throw new Exception("Not found " + typeof(User).ToString());
			else
			{
				users.Remove(item);
				users.Add(entity);
			}

			return entity;
		}
	}
}
