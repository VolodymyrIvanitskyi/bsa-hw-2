﻿using ProjectStructure.DAL.Models;
using ProjectStructure.DAL.UnitOfWork.Interfaces;

namespace ProjectStructure.DAL.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly IRepository<Project> _projectRepository;
        private readonly IRepository<User> _userRepository;
        private readonly IRepository<Team> _teamRepository;
        private readonly IRepository<Models.Task> _taskRepository;

        public UnitOfWork(IRepository<Project> projectRepository,
                            IRepository<User> userRepository,
                            IRepository<Team> teamRepository,
                            IRepository<Models.Task> taskRepository)
        {
            this._projectRepository = projectRepository;
            this._userRepository = userRepository;
            this._teamRepository = teamRepository;
            this._taskRepository = taskRepository;
        }
        public IRepository<Project> ProjectRepository { get => _projectRepository; }

        public IRepository<Team> TeamRepository { get => _teamRepository; }

        public IRepository<Models.Task> TaskRepository { get => _taskRepository; }

        public IRepository<User> UserRepository { get => _userRepository; }
    }
}
