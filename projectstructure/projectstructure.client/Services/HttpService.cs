﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ProjectStructure.Client.Services
{
    public class HttpService
    {
        private static readonly HttpClient _client;
        private const string serverPath = "http://localhost:60756/api/";

        static HttpService()
        {
            _client = new HttpClient();
        }

        public async Task<string> GetEntities(string path)
        {
            HttpResponseMessage response = await _client.GetAsync(serverPath + path);

            if (response.IsSuccessStatusCode)
            {
                string responseBody = await response.Content.ReadAsStringAsync();

                return responseBody;
            }
            else
            {
                //return $"Error: Status code{response.StatusCode}";
                string emessage = response.Content.ReadAsStringAsync().Result;
                throw new Exception(emessage);
            }
        }
    }
}
